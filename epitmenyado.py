tax_rates = {
    "A": 0,
    "B": 0,
    "C": 0
}
telek_adatok = []

# 1. feladat
with open("utca.txt", "r") as f:
    for line in f.readlines():
        line = line.replace("\n", "")
        if tax_rates["A"] != 0:
            telek = line.split(" ")
            telek_adatok.append(
                {
                    "adoszam" : telek[0],
                    "utca": telek[1],
                    "hazszam": telek[2],
                    "kat": telek[3],
                    "nm": int(telek[4])
                }
            )
        else:
            tax_rates["A"], tax_rates["B"], tax_rates["C"] = map(lambda x: int(x), line.split(" "))


# 2. feladat
print(f"2. feladat. A mintában {len(telek_adatok)} telek szerepel.")

# 3. feladat
adoszam = input("Kérem adjon meg egy adószámot >> ")

telkek = filter(lambda x: x["adoszam"] == adoszam, telek_adatok)

if len(telkek) == 0:
    print("Nem szerepel az adatállományban.")
else:
    print(f"3. feladat. Egy tulajdonos adószáma: {adoszam}")
    for telek in telkek:
        print(f'{telek["utca"]} utca {telek["hazszam"]}')


# 4. feladat
def ado(kat, area):
    amount = tax_rates[kat]*area
    return amount if amount >= 10000 else 0


# 5. feladat
ado_and_count = {
    "A" : {
        "count": 0,
        "tax": 0
        }, 
    "B" : {
        "count": 0,
        "tax": 0
        },
    "C" : {
        "count": 0,
        "tax": 0}
    }

for telek in telek_adatok:
    ado_and_count[telek["kat"]]["count"] += 1
    ado_and_count[telek["kat"]]["tax"] += ado(telek["kat"], telek["nm"])

for kat in ado_and_count:
    print(f'{kat} sávba {ado_and_count[kat]["count"]} telek esik, az adó {ado_and_count[kat]["tax"]} Ft.')

# 6. feladat
problem_streets = {}

for telek in telek_adatok:
    if telek["utca"] not in problem_streets:
        problem_streets[telek["utca"]] = set(telek["kat"])
    else:
        if telek["kat"] not in problem_streets[telek["utca"]]:
            problem_streets[telek["utca"]].add(telek["kat"])
            print(telek["utca"])

# 7. feladat
owners = {}

for telek in telek_adatok:
    if telek["adoszam"] in owners:
        owners[telek["adoszam"] ] += ado(telek["kat"], telek["nm"])
    else:
        owners[telek["adoszam"] ] = ado(telek["kat"], telek["nm"])

with open("fizetendo.txt", "w") as f:
    for owner in owners:
        f.write(f"{owner} {owners[owner]}\n")

"""
with open("fizetendo.txt", "w") as f:
    f.writelines(["\n".join([f"{x} {owners[x]}" for x in owners])])
"""
